---
title: Upcoming Ideas
subtitle: Things to do in the future
date: 2020-03-02
tags: ["future"]
draft: true
---
Cool things that I've worked on (for about)

U3 part next
	- Retitle series, or re-subtitle part 1?
	- 2nd part goes into what the barriers are
	- Reservoir of goodwill
	- Affordances
	- The actual benefits
	- Crop images to reduce whitespace around

All you wanted to know about text but were too afraid to ask
	- How long is a piece of string (unicode lengths) - emojis composed of multiple together
	- What does alphabetical/lexographic order mean anymore? (multilingual examples)
	- History of python3 break
	- Byte-order mark
	- Raster tragedy at low resolution
	- Intro to typography

Make a menu item for series

GBA development

Power of the browser
	- jquery get css to hide things for us
	- how this ties into a11y
	- concept of state

Ditching Bootstrap

New 404 page
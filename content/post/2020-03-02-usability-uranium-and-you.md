---
title: Usability, Uranium, and You
subtitle: On the Origin of Standards
date: 2020-03-02
tags: ["ux", "accessibility"]
---

This post is adapted from the first part of a talk I gave in a brown-bag session during my time working for Iglu on the importance of usability and accessibility.

For anyone who entered the world of software development through Computer Science like myself, the focus is typically on the theoretical. This leaves countless fields pertaining to the practical and pragmatic almost entirely untouched. Linguistics, Semiotics, and Psychology are intertwined with the work we carry out each day and failure to embrace this can leave clients and colleagues sore.

<!--more-->

# The Impetus for Usability
Let's take a high-level look at your software right now:

![your app](/img/your-app.png "Your Application")

Your objective as a developer, designer, creator, or whatever your title, is to maximise the output of your application. Most of the time we only concern ourselves with what goes on inside our box - increasing performance, and adding new functionality. However, we will never be able to process the input we never recieve.

![your app](/img/your-app-bad.png "Your Application")

Client interaction is part of our domain, and can't be handwaved away. This model holds not just for the product you are releasing, but the state of the system as it is being created: Source code, development environments, your personal workload. Friction in any of these areas leads to a reduced throughput so we must remain vigilant for any barriers that we may inadvertently raise.

To understand how we might fail this crucial link, let's flip our framing and study a scenario where this failure was the objective.

# Waste Isolation
In 1979, the US Government [funded a study](https://web.archive.org/web/20171130034351/http://www.wipp.energy.gov/picsprog/articles/wipp%20exhibit%20message%20to%2012,000%20a_d.htm) into building a facility for the long-term storage of radioactive materials. "Long-term" referring to 24,000+ years. 15 years later, they realised that maybe they should try to keep people away from it and so commissioned a report on deterring human interference with the site. When you consider that the earliest form of recorded writing that has survived to date is a collection of cuneiform tablets dating back to around 3000 BCE, there are some interesting challenges to overcome:

* Contemporary languages likely won't exist
* Records of current civilisations may not have survived
* Cultural iconography will rot to nonsense
* Physical structures may no longer stand

![proposed structures](/img/proposed-structures.png "Spike Field and Menacing Earthworks")

# Formalise This
Ultimately, the proposals aimed to put up barriers in the following ways:

### Percievability
Any structures or markings are large enough to be seen from far away -- some proposals up-to 2 miles across.
### Operability
Any structures must aim to frustrate people who might want to circumvent the security. In most cases, this meant putting something almost inconceivably directly over the materials.
### Understandable
The design of the structures is to seem unsightly in as visceral a sense as possible to evoke disgust and repel interlopers.
### Robust
Construction materials must be strong enough to last for millenia, but also materially worthless to prevent scavengers.

To relate this back to your application, in 2008 version 2.0 of the [Web Content Accessibility Guideline](https://www.w3.org/TR/WCAG20/) was published. One of the key pieces of this document was the introduction of the four principles referred to today as POUR. By definition, accessible content must be - Percievable, Operable, Understandable, and Robust. These are the core tenets of modern user experience, and they only took 29 years to rediscover.
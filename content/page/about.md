---
title: About Me
subtitle: Full-Stack Developer passionate about delivering value to people
comments: false
---

## Employment History

### Centology: 2020-Present

I have designed and built long-term, extensible solutions to alleviate pressure from a changing business with limited resources. These range from a system which schedules routine tasks as we automate more administration, to designing dashboards which empower stakeholders to reconfigure systems. With such a variety, I also needed to create and maintain quality documentation.

Alongside the regular developer duties, I managed to introduce new practices to the business such as CI/CD, automated testing, and private package hosting. These led to an "incident-free" deployment streak of 18 months and counting!

#### Technologies Used
`C#`, `ASP.NET`, `MVC`, `Sql Server`, 
`TypeScript`, `React`, `CSS`, 
`Azure`, `GitHub`

### Iglu: 2018-2020

The team I worked in was focussed around increasing the efficiency of the call centre and lead-management application. By creating an internal web application to interface with our aggregated supplier API as well as manage passenger and holiday details, we were able to reduce a 45-minute process down to 8.

Through a number of services, we were able to carry out much of the data-gathering before an agent has chance to attend to a lead, allowing them to focus their efforts on the important part - dealing with customers.

#### Technologies Used
`C#`, `ASP.NET`, `MVC`, `Sql Server`, 
`TypeScript`, `React`, `CSS`, 
`AWS`, `TeamCity`, `Octopus Deploy`

### CallVision Technologies: 2013-2018

I started at CallVision a couple of weeks after graduating. Working in a small team afforded a great number of opportunities from formalising a build process, to training support engineers, to delivering my own solution to a 999 call-centre.

At first my role was focussed primarily around operations but, after creating a handful of applications to assist both myself and the support team, I was given more development responsibilities for our commercial products. By the time I left CallVision, I was the primary developer for our CRM/Reporting web application and electron-based softphone.

#### Technologies Used
`C#`, `PHP`, `MySQL`, 
`TypeScript`, `React`, `jQuery`, `CSS`